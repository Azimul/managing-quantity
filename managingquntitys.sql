-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 31, 2015 at 02:49 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `managingquntitys`
--

-- --------------------------------------------------------

--
-- Table structure for table `managingquntity`
--

CREATE TABLE IF NOT EXISTS `managingquntity` (
`id` int(11) NOT NULL,
  `weekday` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `manager` varchar(127) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `managingquntity`
--

INSERT INTO `managingquntity` (`id`, `weekday`, `date`, `manager`, `quantity`) VALUES
(12, 'Fri', '2015-12-03', 'KAJOL', 78),
(13, 'Thu', '2015-12-19', 'SOJOL', 100),
(14, 'Sun', '2015-12-20', 'UJJOL', 100),
(15, 'Wed', '2015-12-15', 'PINTU', 100);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `managingquntity`
--
ALTER TABLE `managingquntity`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `managingquntity`
--
ALTER TABLE `managingquntity`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
