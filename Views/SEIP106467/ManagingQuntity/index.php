<?php
    ini_set('display_errors','Off');
    include_once("../../../vendor/autoload.php");
    
    use App\BITM\SEIP106467\ManagingQuntity\ManagingQuntity;
    
    $managingQuntity = new ManagingQuntity();
    $managingQuntitys = $managingQuntity->index();
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Managing Quntity</title>
        <style>
            #td4 {
                border-color: darkturquoise;
                text-align: end;
            }
        </style>
    </head>
    <body>
        <div style="text-align: right;"><a href="../../../index.html" style="background-color: red; border: 2px; border-width: 35px; border-radius: 10%;">&nbsp;Log Out&nbsp;</a></div>
         
        <center>
         
        <h1> Managing Quantity Table</h1>
       
        <div><span> Search / Filter </span>
            <select>
                <option>5</option>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
            <span>Download as PDF | XL  <b><a href="create.php">Add New</a></b></span>
        </div>
        
        
        <table border="4" >
            <thead style="background-color: blue; color: azure;">
                <th>Weekday</th>
                <th>Date</th>
                <th>Manager</th>
                <th>Quantity</th>
                <th>Action</th>
                
            </thead>
            <tbody >
                <?php
                    $total = 0;
                    foreach($managingQuntitys as $managingQuntity){
                        $date = date("M/d", strtotime($managingQuntity['date']));
                ?>
                  <tr>
                      <td style="background-color: gainsboro;"><?= $managingQuntity['weekday']; ?></td>
                      <td style="background-color: beige;"><?= $date; ?></td>
                      <td style="background-color: seashell;"><?= $managingQuntity['manager']; ?></td>
                      <td id="td4"><?= $managingQuntity['quantity']; ?></td>
                      <td style="background-color: gainsboro;">View | Edit | Delete | Trash/Recover </td>
                  </tr>
                  <?php
                    $total+= $managingQuntity['quantity'];
                    }
                  ?>
                  <tr style="background-color: green; color: yellow;">
                      <th colspan="3">Total</th>
                      <th id="td4"><?= $total; ?></th>
                      <th></th>
                  </tr>
            </tbody>
        </table>
            <div><span> prev  1 | 2 | 3 next </span></div>
     </center>
    </body>
</html>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

